
import org.apache.spark.sql.SparkSession
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter


object vision_uc4 {


  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder().appName("vision_uc4_datamart").master("yarn").enableHiveSupport().getOrCreate()

    val dtf = DateTimeFormatter.ofPattern("yyyyMM")

    val last_month = dtf.format(LocalDateTime.now.minusMonths(1))
    val last_month_one = dtf.format(LocalDateTime.now.minusMonths(2))
    val last_month_two = dtf.format(LocalDateTime.now.minusMonths(3))
    val last_month_three = dtf.format(LocalDateTime.now.minusMonths(4))


// il faut changer les noms des datamarts

    if (args(0).equals("dtm_ini_fusion")) {






      // insertion erbase_om_period_month
      val df_erbase_om_period_month = spark.sql("SELECT * FROM (SELECT MSISDN, transaction_tag, count(*) NB_TRANSACTIONS_ENVOI, sum(0) NB_TRANSACTIONS_RECEPTIONS, count(*) NB_TRANSACTIONS_TOT, sum(MNT_TRANSACTIONS_ENVOI) MNT_TRANSACTIONS_ENVOI, SUM (0) MNT_TRANSACTIONS_RECEPTIONS, sum(MNT_TRANSACTIONS_ENVOI) MONTANT_TRANSACTIONS, sum(CA_TRANSACTIONS_ENVOI) CA_TRANSACTIONS_ENVOI, sum(0) CA_TRANSACTIONS_RECEPTIONS, sum(CA_TRANSACTIONS_ENVOI) CA_TRANSACTIONS,month_id FROM vision_uc4_datalake.dwhdev_erbase_om_period WHERE month_id = "+last_month+" GROUP BY MSISDN, month_id, transaction_tag) t1")


      df_erbase_om_period_month.write.mode("overwrite").format("orc").insertInto("vision_uc4_datalake.dwhdev_erbase_om_period_month_test2")



      val df_dtm_ini_fusion = spark.sql("SELECT tbgom.PERIODE DATE_PERIODE, tbr.MASTER_ID, tbgom.MSISDN NUMERO, tbgom.NOM, tbgom.PRENOMS, tbgom.DATE_NAISSANCE, tbgom.SEX, tbgom.GRADE_NAME, tbgom.PIECE_IDENTITE AS NUM_PIECE_IDENTITE, tbgom.DATE_CREATION, tbr.TYPE_COMPTE, upper(tbr.NOM) NOM_RGST, upper(tbr.PRENOM) PRENOMS_RGST, tbr.DATE_NAISSANCE DATE_NAISSANCE_RGST, tbr.LIEU_NAISSANCE LIEU_NAISSANCE_RGST, tbr.ID_PIECE ID_PIECE_RGST, tbr.TYPE_PIECE TYPE_PIECE_RGST, tbr.DATE_CREATION DATE_CREATION_RGST, tbgom.STATUT_COMPTE AS STATUT_NUMERO, tbomp.TRANSACTION_TAG AS TYPE_TRANSACTIONS, nvl(tbomp.NB_TRANSACTIONS_ENVOI, 0) NB_TRANSACTIONS_ENVOI, nvl(tbomp.NB_TRANSACTIONS_RECEPTIONS, 0) NB_TRANSACTIONS_RECEPTIONS, nvl(tbomp.NB_TRANSACTIONS_TOT, 0) NB_TRANSACTIONS_TOT, nvl(tbomp.MNT_TRANSACTIONS_ENVOI, 0) MNT_TRANSACTIONS_ENVOI, nvl(tbomp.MNT_TRANSACTIONS_RECEPTIONS, 0) MNT_TRANSACTIONS_RECEPTIONS, nvl(tbomp.MONTANT_TRANSACTIONS, 0) MONTANT_TRANSACTIONS, nvl(tbomp.CA_TRANSACTIONS_ENVOI, 0) CA_TRANSACTIONS_ENVOI, nvl(tbomp.CA_TRANSACTIONS_RECEPTIONS, 0) CA_TRANSACTIONS_RECEPTIONS, nvl(tbomp.CA_TRANSACTIONS, 0) CA_TRANSACTIONS, loc.ZONE_COMMERCIALE, loc.NOM_REGION, loc.NOM_DEPART, loc.UNITE_ADMINISTRATIVE, loc.GEOLOCALITE, loc.active_date, CASE WHEN tbr.DATE_NAISSANCE IS NULL THEN 1 ELSE CASE substr(tbr.DATE_NAISSANCE, 1, 10) WHEN substr(tbgom.DATE_NAISSANCE, 1, 10) THEN 1 ELSE 0 END END Well_registrered, "+last_month+" DATE_MONTH FROM (SELECT DISTINCT MSISDN, NOM, PRENOMS, DATE_NAISSANCE, SEX, GRADE_NAME, PIECE_IDENTITE, DATE_CREATION, STATUT_COMPTE, periode FROM vision_uc4_datalake.dwhdev_erbase_globale_omt WHERE periode = (select max(cast(periode as int)) from vision_uc4_datalake.dwhdev_erbase_globale_omt ) ) tbgom LEFT JOIN (SELECT MASTER_ID, id_compte_om, '07' || PHONE_NUM PHONE_NUM, NOM, PRENOM, DATE_NAISSANCE, LIEU_NAISSANCE, ID_PIECE, TYPE_PIECE, DATE_CREATION, TYPE_COMPTE, LIBELLE_TYPESERVICE FROM vision_uc4_datalake.rcu_erbase_referent WHERE LIBELLE_TYPESERVICE = 'ORANGE MONEY' ) tbr ON tbgom.MSISDN = tbr.PHONE_NUM LEFT JOIN (SELECT * FROM vision_uc4_datalake.dwhdev_erbase_om_period_month_test2 WHERE month_id = " + last_month + " ) tbomp ON tbgom.MSISDN = tbomp.MSISDN LEFT JOIN (SELECT MSISDN, MONTH_ID, ACTIVE_DATE, ZONE_COMMERCIALE, NOM_REGION, NOM_DEPART, UNITE_ADMINISTRATIVE, GEOLOCALITE FROM vision_uc4_datalake.csp_oci_g_m_vbm_ft_activite WHERE MONTH_ID = (select max(cast(month_id as int)) from vision_uc4_datalake.csp_oci_g_m_vbm_ft_activite ) ) loc ON 225 || tbgom.MSISDN = loc.MSISDN")

      df_dtm_ini_fusion.write.mode("overwrite").format("orc").insertInto("vision_uc4_datamart.dtm_ini_fusion_test2")




    }

    else if (args(0).equals("ERDTM_FINAL_SHORT")) {



      val df_dtm_uc_nx_compte = spark.sql("SELECT DATE_PERIODE, MASTER_ID, STATUT_MASTER_ID, NOM, PRENOMS, SEX, DATE_NAISSANCE, LIEU_NAISSANCE, NUM_PIECE_IDENTITE, TYPE_PIECE, NB_COMPTE_OM, LIST_COMPTE, LIST_COMPTE_ACTIF, CASE WHEN MASTER_ID IS NULL THEN 'no define' ELSE CASE NB_COMPTE_OM WHEN 1 THEN 'Monoporteurs' ELSE 'Multiporteurs' END END TYPE_PORTEUR_MASTER, NB_TRANSACTIONS_ENVOI, NB_TRANSACTIONS_RECEPTIONS, NB_TRANSACTIONS_TOT, MNT_TRANSACTIONS_ENVOI, MNT_TRANSACTIONS_RECEPTIONS, MONTANT_TRANSACTIONS, CA_TRANSACTIONS_ENVOI, CA_TRANSACTIONS_RECEPTIONS, CA_TRANSACTIONS, CASE NB_TRANSACTIONS_TOT WHEN 0 THEN 0 ELSE 1 END STATUT_ACTIVITE_CLIENT, STATUT_ACTIVITE_CPTE NB_COMPTE_ACTIF, NB_COMPTE_ACTIF_Y, NB_COMPTE_ACTIF_S, ANCIENNETE_MASTER, CASE ANCIENNETE_MASTER WHEN DATE_PERIODE THEN 'PRIMO-SOUSCRIPTEUR' ELSE 'ANCIEN-SOUSCRIPTEUR' END TYPE_SOUSCRIPTION_MASTER, LIST_ZONE_COMMERCIALE, LIST_NOM_REGION, LIST_NOM_DEPART, LIST_UNITE_ADMINISTRATIVE, LIST_GEOLOCALITE, "+last_month+" DATE_MONTH FROM (SELECT DATE_PERIODE, CASE WHEN MASTER_ID IS NULL THEN NOM || PRENOMS || NUM_PIECE_IDENTITE || DATE_NAISSANCE ELSE MASTER_ID END MASTER_ID, CASE WHEN MASTER_ID IS NULL THEN 'no registrered' ELSE 'registrered' END STATUT_MASTER_ID, CASE WHEN MAX(nom_rgst) IS NULL THEN MAX(NOM) ELSE MAX(nom_rgst) END NOM, CASE WHEN MAX(PRENOMS_RGST) IS NULL THEN MAX(PRENOMS) ELSE MAX(PRENOMS_RGST) END PRENOMS, CASE WHEN MAX(cast(date_format(DATE_NAISSANCE_rgst, 'yyyy-MM-dd') AS date)) IS NULL THEN MAX(from_unixtime(unix_timestamp(DATE_NAISSANCE, 'yyyyMMdd'), 'yyyy-MM-dd')) ELSE MAX(cast(date_format(DATE_NAISSANCE_rgst, 'yyyy-MM-dd') AS date)) END DATE_NAISSANCE, MAX(LIEU_NAISSANCE_RGST) LIEU_NAISSANCE, Max(SEX) SEX, CASE WHEN MAX(ID_PIECE_RGST) IS NULL THEN MAX(NUM_PIECE_IDENTITE) ELSE MAX(ID_PIECE_RGST) END NUM_PIECE_IDENTITE, MAX(TYPE_PIECE_RGST) TYPE_PIECE, count(DISTINCT NUMERO) NB_COMPTE_OM, nvl(sum(NB_TRANSACTIONS_ENVOI), 0) NB_TRANSACTIONS_ENVOI, nvl(sum(NB_TRANSACTIONS_RECEPTIONS), 0) NB_TRANSACTIONS_RECEPTIONS, nvl(sum(NB_TRANSACTIONS_TOT), 0) NB_TRANSACTIONS_TOT, nvl(sum(MNT_TRANSACTIONS_ENVOI), 0) MNT_TRANSACTIONS_ENVOI, nvl(sum(MNT_TRANSACTIONS_RECEPTIONS), 0) MNT_TRANSACTIONS_RECEPTIONS, nvl(sum(MONTANT_TRANSACTIONS), 0) MONTANT_TRANSACTIONS, nvl(sum(CA_TRANSACTIONS_ENVOI), 0) CA_TRANSACTIONS_ENVOI, nvl(sum(CA_TRANSACTIONS_RECEPTIONS), 0) CA_TRANSACTIONS_RECEPTIONS, nvl(sum(CA_TRANSACTIONS), 0) CA_TRANSACTIONS, substr(replace(CASE WHEN MIN(DATE_CREATION) IS NULL THEN MIN(DATE_CREATION_RGST) ELSE MIN(DATE_CREATION) END, '-', ''), 1, 6) ANCIENNETE_MASTER FROM vision_uc4_datamart.dtm_ini_fusion_test2  where date_month=(select max(cast (date_month as int)) from vision_uc4_datamart.dtm_ini_fusion_test2) GROUP BY DATE_PERIODE, CASE WHEN MASTER_ID IS NULL THEN NOM || PRENOMS || NUM_PIECE_IDENTITE || DATE_NAISSANCE ELSE MASTER_ID END, CASE WHEN MASTER_ID IS NULL THEN 'no registrered' ELSE 'registrered' END) tb_1 LEFT JOIN (SELECT MASTER_ID mt, DATE_PERIODE dt_perd, split(regexp_replace(concat_ws(',', sort_array(collect_list(concat_ws(':', date_format(date_creation, 'yyyy-MM-dd'), cast(NUMERO AS string))))), '[^:]*:([^,]*(,|$))', '$1'), ',') AS LIST_COMPTE, split(regexp_replace(concat_ws(',', sort_array(collect_list(concat_ws(':', date_format(date_creation, 'yyyy-MM-dd'), cast((CASE NB_TRANSACTIONS_TOT WHEN 0 THEN 0 ELSE 1 END) AS string))))), '[^:]*:([^,]*(,|$))', '$1'), ',') AS LIST_COMPTE_ACTIF, sort_array(collect_list(date_creation)) AS date_creation, sum(CASE NB_TRANSACTIONS_TOT WHEN 0 THEN 0 ELSE 1 END) STATUT_ACTIVITE_CPTE, sum(CASE STATUT_NUMERO WHEN 'Y' THEN CASE NB_TRANSACTIONS_TOT WHEN 0 THEN 0 ELSE 1 END ELSE 0 END) NB_COMPTE_ACTIF_Y, sum(CASE STATUT_NUMERO WHEN 'S' THEN CASE NB_TRANSACTIONS_TOT WHEN 0 THEN 0 ELSE 1 END ELSE 0 END) NB_COMPTE_ACTIF_S FROM (SELECT (CASE WHEN MASTER_ID IS NULL THEN NOM || PRENOMS || NUM_PIECE_IDENTITE || DATE_NAISSANCE ELSE MASTER_ID END) MASTER_ID, DATE_PERIODE, NUMERO, STATUT_NUMERO, min(DATE_CREATION) DATE_CREATION, nvl(sum(NB_TRANSACTIONS_TOT), 0) NB_TRANSACTIONS_TOT FROM vision_uc4_datamart.dtm_ini_fusion_test2 WHERE DATE_month = (select max(cast (date_month as int)) from vision_uc4_datamart.dtm_ini_fusion_test2) GROUP BY CASE WHEN MASTER_ID IS NULL THEN NOM || PRENOMS || NUM_PIECE_IDENTITE || DATE_NAISSANCE ELSE MASTER_ID END, NUMERO, DATE_PERIODE, STATUT_NUMERO) h GROUP BY MASTER_ID, DATE_PERIODE) tb_2 ON tb_1.master_id = tb_2.mt AND tb_1.DATE_PERIODE = tb_2.dt_perd LEFT JOIN (SELECT * FROM (SELECT MASTER_ID mtloc, DATE_PERIODE dt_perid, ZONE_COMMERCIALE LIST_ZONE_COMMERCIALE, NOM_REGION LIST_NOM_REGION, NOM_DEPART LIST_NOM_DEPART, UNITE_ADMINISTRATIVE LIST_UNITE_ADMINISTRATIVE, GEOLOCALITE LIST_GEOLOCALITE, row_number() OVER (PARTITION BY MASTER_ID, DATE_PERIODE ORDER BY DATE_CREATION DESC) rank_id FROM (SELECT CASE WHEN MASTER_ID IS NULL THEN NOM || PRENOMS || NUM_PIECE_IDENTITE || DATE_NAISSANCE ELSE MASTER_ID END MASTER_ID, DATE_PERIODE, NUMERO, ZONE_COMMERCIALE, NOM_REGION, NOM_DEPART, UNITE_ADMINISTRATIVE, GEOLOCALITE, min(DATE_CREATION) DATE_CREATION FROM vision_uc4_datamart.dtm_ini_fusion_test2 WHERE  date_month=(select max(cast (date_month as int)) from vision_uc4_datamart.dtm_ini_fusion_test2) GROUP BY CASE WHEN MASTER_ID IS NULL THEN NOM || PRENOMS || NUM_PIECE_IDENTITE || DATE_NAISSANCE ELSE MASTER_ID END, NUMERO, DATE_PERIODE, ZONE_COMMERCIALE, NOM_REGION, NOM_DEPART, UNITE_ADMINISTRATIVE, GEOLOCALITE) v) f WHERE rank_id = 1 ) tb_loc ON tb_1.master_id = tb_loc.mtloc AND tb_1.DATE_PERIODE = tb_loc.dt_perid")

      df_dtm_uc_nx_compte.write.mode("overwrite").format("orc").insertInto("vision_uc4_datamart.ERDTM_FINAL_SHORT_test2")

    }
    else if (args(0).equals("ERdtm_uc_nx_compte1")) {

      val df_erdtm_uc_nx_compte1 = spark.sql("select idd.DATE_PERIODE, idd.MASTER_ID, idd.ZONE_COMMERCIALE, idd.NOM_REGION, idd.NOM_DEPART, idd.UNITE_ADMINISTRATIVE, idd.NUMERO, idd.ACTIVE_DATE, idd.DATE_CREATION, idd.DATE_CREATION_RGST, idd.LIST_TRANSACTIONS, idd.CA_TRANSACTIONS, idd.MNT_TRANSACTIONS, j.DATE_PERIODEj, j.master_idj, j.statut_master_id, j.type_souscription_master, idd.DATE_MONTH from ( select DATE_PERIODE, case when MASTER_ID IS NULL then NOM || PRENOMS || NUM_PIECE_IDENTITE || DATE_NAISSANCE else MASTER_ID end MASTER_ID, ZONE_COMMERCIALE, NOM_REGION, NOM_DEPART, UNITE_ADMINISTRATIVE, NUMERO, ACTIVE_DATE, substr(replace( case when DATE_CREATION IS NULL then DATE_CREATION_RGST else DATE_CREATION end , '-', ''), 1, 8) DATE_CREATION, min(DATE_CREATION_RGST) DATE_CREATION_RGST, collect_list(TYPE_TRANSACTIONS) over (partition by date_creation order by date_creation) as LIST_TRANSACTIONS, collect_list(CA_TRANSACTIONS) over (partition by date_creation order by date_creation) as CA_TRANSACTIONS, collect_list(MONTANT_TRANSACTIONS) over (partition by date_creation order by date_creation) as MNT_TRANSACTIONS, " + last_month + " DATE_MONTH from vision_uc4_datamart.dtm_ini_fusion_test2 where DATE_PERIODE =(select max(cast (DATE_PERIODE as int)) from vision_uc4_datamart.dtm_ini_fusion_test2) and substr(replace( case when DATE_CREATION IS NULL then DATE_CREATION_RGST else DATE_CREATION end , '-', ''), 1, 6) = DATE_PERIODE group by DATE_PERIODE, ZONE_COMMERCIALE, NOM_REGION, NOM_DEPART, UNITE_ADMINISTRATIVE, NUMERO, ACTIVE_DATE, case when MASTER_ID IS NULL then NOM || PRENOMS || NUM_PIECE_IDENTITE || DATE_NAISSANCE else MASTER_ID end , substr(replace( case when DATE_CREATION IS NULL then DATE_CREATION_RGST else DATE_CREATION end , '-', ''), 1, 8), DATE_CREATION , TYPE_TRANSACTIONS, CA_TRANSACTIONS, MONTANT_TRANSACTIONS ) idd left join ( select DATE_PERIODE DATE_PERIODEj, master_id master_idj, statut_master_id, type_souscription_master from vision_uc4_datamart.ERDTM_FINAL_SHORT_test2 where DATE_PERIODE =(select max(cast (DATE_PERIODE as int)) from vision_uc4_datamart.ERDTM_FINAL_SHORT_test2) ) j on idd.master_id = j.master_idj and idd.DATE_PERIODE = j.DATE_PERIODEj")

      df_erdtm_uc_nx_compte1.write.mode("overwrite").format("orc").insertInto("vision_uc4_datamart.erdtm_uc_nx_compte1_test")

    }
    else if (args(0).equals("DTM_INACTIF_POTENTIEL")) {

      val df_DTM_INACTIF_POTENTIEL = spark.sql("select DATE_PERIODE, MASTER_ID, STATUT_MASTER_ID, ANCIENNETE_MASTER, TYPE_PORTEUR_MASTER, LIST_ZONE_COMMERCIALE, LIST_NOM_REGION, LIST_NOM_DEPART, LIST_UNITE_ADMINISTRATIVE, LIST_GEOLOCALITE, NB_COMPTE_OM, LIST_COMPTE, LIST_COMPTE_ACTIF, LST_COMPTE_PREW, LIST_COMPTE_ACTIF_PREW, MONTANT_TRANSACTIONS_M1, CA_TRANSACTIONS_M1, MONTANT_TRANSACTIONS_M2, CA_TRANSACTIONS_M2, MONTANT_TRANSACTIONS_M3, CA_TRANSACTIONS_M3, " + last_month + " DATE_MONTH from ( select DATE_PERIODE, MASTER_ID, STATUT_MASTER_ID, ANCIENNETE_MASTER, TYPE_PORTEUR_MASTER, LIST_ZONE_COMMERCIALE, LIST_NOM_REGION, LIST_NOM_DEPART, LIST_UNITE_ADMINISTRATIVE, LIST_GEOLOCALITE, NB_COMPTE_OM, LIST_COMPTE, LIST_COMPTE_ACTIF from vision_uc4_datamart.ERDTM_FINAL_SHORT_test2 where DATE_PERIODE = " + last_month + " and STATUT_ACTIVITE_CLIENT = 0 ) tb_base LEFT JOIN ( select MASTER_ID MASTER_ID_INACTIF, LIST_COMPTE LST_COMPTE_PREW, LIST_COMPTE_ACTIF LIST_COMPTE_ACTIF_PREW, MONTANT_TRANSACTIONS MONTANT_TRANSACTIONS_M1, CA_TRANSACTIONS CA_TRANSACTIONS_M1 from vision_uc4_datamart.ERDTM_FINAL_SHORT_test2 where DATE_PERIODE = " + last_month_one + " and STATUT_ACTIVITE_CLIENT = 1 and LIST_COMPTE is not NULL ) tb_base_prew on tb_base.MASTER_ID = tb_base_prew.MASTER_ID_INACTIF LEFT JOIN ( select MASTER_ID MASTER_ID_INACTIF2, MONTANT_TRANSACTIONS MONTANT_TRANSACTIONS_M2, CA_TRANSACTIONS CA_TRANSACTIONS_M2 from vision_uc4_datamart.ERDTM_FINAL_SHORT_test2 where DATE_PERIODE = " + last_month_two + " ) tb_base_prew_2 on tb_base.MASTER_ID = tb_base_prew_2.MASTER_ID_INACTIF2 LEFT JOIN ( select MASTER_ID MASTER_ID_INACTIF3, MONTANT_TRANSACTIONS MONTANT_TRANSACTIONS_M3, CA_TRANSACTIONS CA_TRANSACTIONS_M3 from vision_uc4_datamart.ERDTM_FINAL_SHORT_test2 where DATE_PERIODE = " + last_month_three+ " ) tb_base_prew_3 on tb_base.MASTER_ID = tb_base_prew_3.MASTER_ID_INACTIF3")
      df_DTM_INACTIF_POTENTIEL.write.mode("overwrite").format("orc").insertInto("vision_uc4_datamart.dtm_inactif_potentiel_test")



    }



    spark.stop()
    spark.close()


  }
}